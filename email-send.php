<?php
require_once "vendor/autoload.php";
$errorMSG = "";


/* NAME */
if (empty($_POST["nombre"])) {
    $errorMSG = "<span> . El nombre es necesario</span>";
} else {
    $name = $_POST["nombre"];
}

/* EMAIL */
if (empty($_POST["email"])) {
    $errorMSG .= "<span> . El email es requerido</span>";
} else if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
    $errorMSG .= "<span> . El email es incorrecto</span>";
}else {
    $email = $_POST["email"];
}

/* MESSAGE */
if (empty($_POST["mensaje"])) {
    $errorMSG .= "<span> . El mensaje es requerido</span>";
} else {
    $message = $_POST["mensaje"];
}

if(empty($errorMSG)){
	$msg = enviar_email();
	echo json_encode(['code'=>200, 'msg'=>"<<<< ".$msg." >>>>"]);
}else{
    echo json_encode(['code'=>404, 'msg'=>"<<<< ".$errorMSG." >>>>"]);
}

function enviar_email(){
    //PHPMailer Object
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    //To address and name
    $mail->addAddress("info@maumach.com.ar", "Email WEB");
    $mail->addAddress("mauro@maumach.com.ar", "Email WEB"); //Recipient name is optional
    //Address to which recipient will reply
    $mail->addReplyTo("info@maumach.com.ar", "Reply");
    //CC and BCC
    $mail->addCC("sistemas.mauro@gmail.com");
    //Send HTML or Plain Text email
    $mail->isHTML(true);
    //From email address and name
    $mail->From = @$_POST["email"];
    $mail->FromName = "EMail de CONTACTO web MAUMACH.COM.AR";
    $mail->Subject = "Consulta a través de la web MAUMACH.COM.AR";
    $mail->Body = "<h3>NOMBRE : ".$_POST["nombre"]."</h3>"."<h3>ASUNTO: ".@$_POST["asunto"]."</h3>"."<h3>EMAIL: ".@$_POST["email"]."</h3>"."<h4><b>MENSAJE:</b> <br>".@$_POST["mensaje"]."</h4>";
    $mail->AltBody = @$_POST["asunto"];

    if(!$mail->send())
    {
        return "Error al enviar el mensaje";
    }
    else
    {
        return "Mensaje enviado correctamente";
    }
}
die;
?>
